import React from 'react'
import Torch from 'react-native-torch'
import {View, Button} from 'react-native'

export default class testFlash extends React.Component {

    state ={
        isPress: false
    }

    handlePress() {
        const {isPress} = this.state;
        Torch.switchState(!isPress);
        this.setState({isPress: !isPress});
    }

    render() {
        return(
            <View>
                <Button onPress={this.handlePress.bind(this)} title="Flash"/>
            </View>
        );
    }
}