/** @format */

import {AppRegistry} from 'react-native';
// import App from './App';
import testFlash from './testFlash'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => testFlash);
